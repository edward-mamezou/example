package com.pigumer.download;

import com.pigumer.model.Settings;
import com.pigumer.parser.Parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Download {

    public static final String name = "build/openapi-generator-cli.jar";

    public void download(String file) throws IOException, InterruptedException {
        try (FileInputStream in = new FileInputStream(Paths.get(file).toFile())) {
            Parser parser = new Parser();
            Settings settings = parser.parse(in);
            String openApiGeneratorVersion = settings.getOpenAPIGeneratorVersion();
            String url = String.format("https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/%s/openapi-generator-cli-%s.jar", openApiGeneratorVersion, openApiGeneratorVersion);
            HttpClient client = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.ALWAYS).build();
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create(url)).GET().build();
            HttpResponse<byte[]> response = client.send(request, BodyHandlers.ofByteArray());
            if (response.statusCode() == 200) {
                Files.write(Paths.get(name), response.body());
            }
        }
    }
}
