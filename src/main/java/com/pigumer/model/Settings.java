package com.pigumer.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Settings {

    // https://docs.oracle.com/javase/specs/jls/se11/html/jls-3.html#jls-Identifier
    private final Set<String> keywords = Set.of("abstract", "continue", "for", "new", "switch",
            "assert", "default", "if", "package", "synchronized",
            "boolean", "do", "goto", "private", "this",
            "break", "double", "implements", "protected", "throw",
            "byte", "else", "import", "public", "throws",
            "case", "enum", "instanceof", "return", "transient",
            "catch", "extends", "int", "short", "try",
            "char", "final", "interface", "static", "void",
            "class", "finally", "long", "strictfp", "volatile",
            "const", "float", "native", "super", "while", "_");
    private final Set<String> booleanLiteral = Set.of("true", "false");
    private final Set<String> nullLiteral = Set.of("null");

    private String version;
    private String openAPIGeneratorVersion;
    private Map<String, String> environment;
    private List<Module> modules;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Map<String, String> getEnvironment() {
        return environment;
    }

    protected void validatePackageName(String pkg) {
        if (pkg.startsWith(".") || pkg.endsWith(".")) {
            throw new RuntimeException("パッケージ名が不正です: " + pkg);
        }
        String[] pkgs = pkg.split("\\.");
        for (String identifier : pkgs) {
            if (identifier.isEmpty()) {
                throw new RuntimeException("パッケージ名に空文字列が含まれています");
            }
            if (keywords.contains(identifier) || booleanLiteral.contains(identifier) || nullLiteral.contains(identifier)) {
                throw new RuntimeException("パッケージ名に使用できないキーワードが含まれています: " + identifier);
            }
            if (!Character.isJavaIdentifierStart(identifier.charAt(0))) {
                throw new RuntimeException("パッケージ名に使用できない開始文字が含まれています: " + identifier);
            }
            for (int c : identifier.substring(1).toCharArray()) {
                if (!Character.isJavaIdentifierPart(c)) {
                    throw new RuntimeException("パッケージ名に使用できない文字が含まれています: " + identifier);
                }
            }
        }
    }

    public void setEnvironment(Map<String, String> environment) {
        this.environment = environment;
        if (environment.containsKey("Package")) {
            validatePackageName(environment.get("Package"));
        }
    }

    public String getOpenAPIGeneratorVersion() {
        return openAPIGeneratorVersion;
    }

    public void setOpenAPIGeneratorVersion(String openAPIGeneratorVersion) {
        this.openAPIGeneratorVersion = openAPIGeneratorVersion;
    }

    public List<Module> getModules() {
        return modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }
}
