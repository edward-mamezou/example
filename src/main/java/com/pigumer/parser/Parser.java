package com.pigumer.parser;

import com.pigumer.model.Settings;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class Parser {

    public Settings parse(InputStream in) {
        Yaml yaml = new Yaml();
        Settings settings = yaml.loadAs(in, Settings.class);
        return settings;
    }

    public Map<String, String> properties(Settings settings) {
        Map<String, String> properties = settings.getEnvironment();
        properties.put("now", DateTimeFormatter.ofPattern("uuuuMMddHHmmss").format(ZonedDateTime.now(ZoneId.of("UTC"))));
        Map<String, String> env = System.getenv();
        properties.putAll(env);
        return properties;
    }
}
