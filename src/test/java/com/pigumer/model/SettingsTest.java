package com.pigumer.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SettingsTest {

    private Settings settings = new Settings();

    @Test
    public void packageNameTest() {
        settings.validatePackageName("com.example_example");
        RuntimeException ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName(""));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("."));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com."));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com..example"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.123"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.ex-ample"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.goto"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.true"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.false"));
        System.out.println(ex.getMessage());
        ex = Assertions.assertThrows(RuntimeException.class, () -> settings.validatePackageName("com.null"));
        System.out.println(ex.getMessage());
    }
}
